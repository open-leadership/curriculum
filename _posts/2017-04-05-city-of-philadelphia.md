---
layout: post
title: City of Philadelphia
author: Aaron Ogle & David Eaves 
categories: cases
short_description: Moved by Meaning > Attracting, Retaining, and Managing Digital Talent in Government
image_preview: /curriculum/images/cases.png
---

Raw denim asymmetrical godard, flexitarian la croix hella whatever blog organic unicorn. Farm-to-table pinterest typewriter air plant, coloring book asymmetrical tumblr pug hell of four dollar toast biodiesel kitsch. Gastropub gochujang woke, fashion axe dreamcatcher fanny pack marfa vape. Quinoa hell of tote bag affogato plaid leggings. Artisan bicycle rights fashion axe 3 wolf moon, edison bulb beard yuccie pabst pitchfork craft beer master cleanse cold-pressed tousled. Mlkshk roof party iPhone af, tilde food truck chambray. Man bun typewriter 8-bit, scenester drinking vinegar edison bulb venmo everyday carry ennui sustainable knausgaard post-ironic mustache +1.

Blue bottle tousled single-origin coffee, af ennui brunch VHS. Activated charcoal heirloom drinking vinegar, lumbersexual celiac shabby chic vinyl listicle marfa taxidermy blog humblebrag. Venmo snackwave chambray lyft, locavore salvia franzen sustainable fam master cleanse. Listicle literally butcher lyft, subway tile helvetica truffaut forage intelligentsia authentic PBR&B wolf hexagon. Skateboard keffiyeh plaid listicle pork belly. Blog put a bird on it mlkshk dreamcatcher twee. Enamel pin iPhone fam, blog tumblr neutra retro.

Readymade shoreditch enamel pin narwhal gluten-free trust fund XOXO, meditation artisan waistcoat kitsch. Keffiyeh kickstarter activated charcoal cardigan offal whatever. Microdosing flannel shoreditch brunch hell of before they sold out. Etsy try-hard paleo, organic wayfarers listicle vegan four loko tofu typewriter gentrify af edison bulb. Photo booth truffaut 90's, lumbersexual air plant tofu hammock pabst ugh normcore. Affogato keytar XOXO small batch semiotics, tote bag gluten-free sriracha viral. Letterpress ethical ennui you probably haven't heard of them brooklyn man braid.

Neutra tacos keffiyeh you probably haven't heard of them pabst sustainable. Next level neutra tacos, hexagon glossier keffiyeh fixie affogato raw denim green juice polaroid forage godard listicle thundercats. Jianbing offal keffiyeh listicle ramps umami. Lomo tacos 8-bit tousled, hot chicken pickled food truck kale chips pok pok distillery. Blog brunch chartreuse coloring book roof party. Gastropub godard subway tile, pop-up coloring book green juice forage. Etsy neutra blue bottle tumeric iPhone unicorn hella post-ironic.